package com.sblp;

public class APIName {

	// version
	public static final String VERSION = "api/v1/";
	// charset
	public static final String CHARSET = "application/json;charset=utf-8";

	// action admin
	public static final String ADMIN = VERSION + "admin";
	public static final String ADMIN_LOGIN = ADMIN + "/login";
	
	
	// action Product
	public static final String PRODUCT = VERSION + "product";
	public static final String SAVE_PRODUCT = PRODUCT + "/saveProduct";
	public static final String GET_PRODUCT_LIST = PRODUCT + "/getProducts";
	public static final String GET_PRODUCT_BY_PDTCODE = PRODUCT + "/getProductByPdtCode";
	public static final String GET_PRODUCT_BY_PDTTYPE = PRODUCT + "/getProductByPdtType";
	
	// action ProductType
	public static final String PRODUCT_TYPE = VERSION + "productType";
	public static final String SAVE_PRODUCT_TYPE = PRODUCT_TYPE + "/saveProductType";
	public static final String GET_PRODUCT_TYPE_LIST = PRODUCT_TYPE + "/getProductTypes";
	public static final String GET_PRODUCT_TYPE_BY_PDTCODE = PRODUCT_TYPE + "/getProductByPdtypeCode";
	
	//action productCategory
	public static final String PRODUCT_CATEGORY = VERSION + "productCategory";
	public static final String SAVE_PRODUCT_CATEGORY = PRODUCT_CATEGORY + "/saveProductCategory";
	public static final String GET_PRODUCT_CATEGORY_LIST = PRODUCT_CATEGORY + "/getProductCategorys";
	public static final String GET_PRODUCT_CATEGORY_WITH_SUB_CATEGORY_LIST = PRODUCT_CATEGORY + "/getProductCategorysWithSubCategories";
	
	//action ProductSubCategory
	public static final String PRODUCT_SUB_CATEGORY = VERSION + "ProductSubCategory";
	public static final String SAVE_PRODUCT_SUB_CATEGORY = PRODUCT_SUB_CATEGORY + "/saveProductSubCategory";
	public static final String GET_PRODUCT_SUB_CATEGORY_LIST = PRODUCT_SUB_CATEGORY + "/getProductSubCategorys";
	
	//action ProductSubCategory
	public static final String STORES = VERSION + "Store";
	public static final String SAVE_STORE = STORES + "/saveStore";
	public static final String GET_STORE_LIST = STORES + "/getStoreList";
}
